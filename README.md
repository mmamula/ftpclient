# FTP Client

## 1. Usage

Run with:
	  
```
java FTPClient -u USER -p PASSWORD -server SERVER -files "TARGET;DESTINATION" "TARGET;DESTINATION"
```

## 2. Notes

Tested on tnFTPd, proFTPd and vsFTPd FTP servers

\33[1A escape sequence used can sometimes break cli output on windows