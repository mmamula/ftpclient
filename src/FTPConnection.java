import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FTPConnection implements Runnable {
	
	public SocketWrapper connectionSocket;
	public SocketWrapper dataSocket;
	
	/*
	 * Connection and upload file data
	 */
	public String host;
	public int port;
	public String username;
	public String password;
	public String fileFrom;
	public String fileTo;
	
	/*
	 * Transfer tracking props
	 */
	public String status;
	public long transferStart;
	public long transferEnd;
	public int transferBytesSent;
	public boolean transferInProgress;
	
	
	public FTPConnection (String host, int port, String username, String password, String fileFrom, String fileTo) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.fileFrom = fileFrom;
		this.fileTo = fileTo;
		transferInProgress = false;
		status = "Preparing to send:" + fileFrom + " to " + host + ":" + fileTo;
	}
	
	/*
	 * Main transaction method
	 */
	public void sendData () throws IOException, InterruptedException {
		boolean flow = false;
		try {
			flow = openConnection() 
				&& logIn()
				&& setRepresentationType()
				&& setPASVconnection()
				&& uploadFile();
		} catch (SocketException e) {
			transferInProgress = false;
			status = fileFrom + " can't be uploaded because server or client terminated connection";
		}
		if (flow) {
			float totalTime = (transferEnd - transferStart) / 1000;
			double totalKb = (double) transferBytesSent / 1000;
			double kbs = (transferBytesSent / 1000) / (totalTime != 0 ? totalTime : 1);
			status = fileFrom + " Uploaded | size: " + totalKb + " kB at " + 
					(kbs > 0 ? kbs : totalKb)  + " kB/s | finished in " + totalTime + "s";
		}
	}
	
	/*
	 * Open socket and validate FTP response
	 */
	protected boolean openConnection () throws IOException, UnknownHostException {
		try {
			connectionSocket = new SocketWrapper(host, port);
			String initialResponse = connectionSocket.in.readLine();
			if (!initialResponse.substring(0, 3).equals("220")) {
				status = fileFrom + " -> Can't establish connection with FTP server";
				return false;
			}
		} catch (ConnectException e) {
			status = fileFrom + " -> Can't establish connection with host";
			return false;
		}
		return true;
	}
	
	/*
	 * Send credentials to server
	 */
	protected boolean logIn () throws IOException, UnknownHostException {
		connectionSocket.input("USER " + username);
		String response = connectionSocket.in.readLine();
		if (response.substring(0, 3).equals("331")) { // Password is required
			connectionSocket.input("PASS " + password);
			response = connectionSocket.in.readLine();
		}
		if (!response.substring(0, 3).equals("230")) { // Invalid password or not allowed to log in
			status = fileFrom + " -> Can't log in to server";
			return false;
		}
		return true;
	}
	
	/*
	 * Set representation type for file transfer
	 */
	protected boolean setRepresentationType () throws IOException {
		String type = FTPHelper.getRepresentationType(fileFrom);
		connectionSocket.input("TYPE " + type);
		String response = connectionSocket.in.readLine();
		if (response.charAt(0) == '5' && type.charAt(0) != 'I') {
			// Set I as representation type if other not implemented
			connectionSocket.input("TYPE I");
			response = connectionSocket.in.readLine();
		}
		if (!(response.charAt(0) == '2')) {
			status = fileFrom + " -> Can't negotiate representation type";
			return false;
		}
		return true;
	}
	
	/*
	 * Ask server for data socket in PASV mode and connect to it
	 */
	protected boolean setPASVconnection () throws IOException {
		connectionSocket.input("PASV");
		String pasvResponse = connectionSocket.in.readLine();
		String pattern = "(\\d*,\\d*,\\d*,\\d*),(\\d*),(\\d*)";
		 Pattern r = Pattern.compile(pattern);
		 Matcher m = r.matcher(pasvResponse);
		 if (!m.find()) {
			 status = fileFrom + " -> Can't get ip addres and port for data transfer";
		 	 return false;
	     } 
		 dataSocket = new SocketWrapper(
			 m.group(1).replace(',', '.'), 
			 FTPHelper.parsePortFromHex(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)))
		 );
		return true;
	}
	
	/*
	 * File upload to server
	 */
	protected boolean uploadFile () throws IOException, SocketException {
		connectionSocket.input("STOR " + fileTo);
		String response = connectionSocket.in.readLine();
		if (!response.substring(0, 3).equals("150")) {
			status = fileFrom + " -> Can't start data transfer";
			return false;
		}
		File file = new File(fileFrom);
		DataInputStream fileReader 	= new DataInputStream(new FileInputStream(file));
		transferStart = System.currentTimeMillis();
		transferBytesSent = 0;
		transferInProgress = true;
		try {
			while (true) {
				transferBytesSent++;
				dataSocket.socket.getOutputStream().write((int) fileReader.readByte());
			}	
		} catch (EOFException e) {
			fileReader.close();		
		}
		transferInProgress = false;
		transferEnd = System.currentTimeMillis();
		dataSocket.socket.close();
		response = connectionSocket.in.readLine();
		if (!response.substring(0, 3).equals("226")) {
			status = "Failed to upload file: " + fileFrom;
			return false;
		}
		return true;
	}
	
	/*
	 * Update status which is displayed in console
	 */
	public void updateStatus () {
		if (transferInProgress) {
			long current; 
			long elapsedTime;
			long kBSent;
			long kbs;
			current = System.currentTimeMillis();
			elapsedTime = (current - transferStart) / 1000;
			kBSent = (transferBytesSent / 1000);
			kbs = kBSent / (elapsedTime > 0 ? elapsedTime : 1);
			status = fileFrom + " total s: " + (elapsedTime) + " bytes: " + 
				(kBSent > 0 ? kBSent : "1 > ") + "kB" +
				"  ---  " + kbs + " kB/s";	
		}
	}
	
	/*
	 * Close sockets
	 */
	public void close () throws IOException {
		if (connectionSocket != null && connectionSocket.socket != null) {
			connectionSocket.socket.close();
		}
		if (dataSocket != null && dataSocket.socket != null) {
			dataSocket.socket.close();	
		}
	}

	@Override
	public void run() {
		try {
			sendData();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
