import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class FTPClient {

	public static void main(String[] args) throws InterruptedException, UnknownHostException {
		try {
			
			/**
			 * Set default values
			 */
			List<String> keysList = new ArrayList<String>();
			keysList.add("-u");
			keysList.add("-p");
			keysList.add("-server");
			keysList.add("-files");
			String user = "user";
			String password = "pass";
			String server = "127.0.0.1";
			List<String> files = new ArrayList<String>();
			
			/**
			 * Parse cli arguments
			 */
			for (int i = 0; args.length > i; i++) {
				int next = i + 1;
				if (args.length > next && !keysList.contains(args[next])) {
					if (args[i].equals("-u")) {
						user = args[next]; 
					} else if (args[i].equals("-p")) {
						password = args[next];	
					} else if (args[i].equals("-server")) {
						server = args[next];
					} else if (args[i].equals("-files")) {
						for (int j = next; j < args.length; j++) {
							files.add(args[j]);
						}
					}
				}				
			}
			
			if (files.size() < 1) {
				System.out.println("No files selected for upload.");
				return;
			}
			
			FTPController controller = new FTPController();
			
			/**
			 * Create FTP connections and add them to controller
			 */
			int filesSize = files.size();
			for (int i = 0; filesSize > i; i++) {
				String[] filePair = files.get(i).split(";");
				if (filePair.length == 2) {
					controller.addFtpConnection(new FTPConnection(server, 21, user, password, filePair[0], filePair[1]));	
				} else {
					System.out.println("ERROR: " + files.get(i) + " is not valid file pair arg");
				}
			}
			
			/**
			 * Start sending files to FTP server
			 */
			if (controller.ftpConnections.size() > 0) {
				controller.executeTransfers();
				Thread t = new Thread(controller, "controller");
				t.start();
				controller.joinAll();
				controller.outputStatus = false; // stop writing to console
				t.join();
				controller.printStatus();
				for (int i = 0; i < controller.ftpConnections.size(); i++) {
					System.out.print("\n");	
				}
				controller.closeFtpConnections();	
			}
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
