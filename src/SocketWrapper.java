import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketWrapper {
	
	public Socket socket;
	public BufferedReader in;
	public PrintWriter out;

	public SocketWrapper (String host, int port) throws IOException, UnknownHostException {
		socket = new Socket(host, port);
		setupInOut();
	}
	
	public SocketWrapper (Socket socket) throws IOException {
		this.socket = socket;
		setupInOut();
	}
	
	private void setupInOut () throws IOException {
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);	
	}
	
	public void output () throws IOException {
		System.out.println(in.readLine());
	}	

	public void input (String data) throws IOException {
		out.println(data);
	}
	
}
