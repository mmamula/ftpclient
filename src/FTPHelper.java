import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FTPHelper {
	
	private static String[] textTypes = {"txt"};
	private static String[] imageTypes = {"img", "jpg", "png", "gif"};
	
	public static String getRepresentationType (String file) {
		String pattern = "\\.([A-Za-z\\d]*)$";
	 	Pattern r = Pattern.compile(pattern);
	 	Matcher m = r.matcher(file);
	 	if (m.find()) {
	 		String match = m.group(1);
	 		if (Arrays.asList(textTypes).contains(match)) {
	 			return "A";
	 		} else if (Arrays.asList(imageTypes).contains(match)) {
	 			return "I";
	 		}
	    } 
		return "L"; // Default to binary file type
	}
	
	/**
	 * Decide port number from PASV response
	 */
	public static int parsePortFromHex (int first, int second) {
		String hex1 = Integer.toHexString(first);
		String hex2 = Integer.toHexString(second);
		if (hex2.length() < 2) {
			hex2 = "0" + hex2;
		}
		return Integer.parseInt(hex1 + hex2, 16);
	}
	
}
