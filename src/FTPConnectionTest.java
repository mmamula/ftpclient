import static org.junit.Assert.*;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FTPConnectionTest {
	
	public FTPConnectionExtend instance;
	public int port;
	public Server server;
	public Thread serverThead;
	
	@Before
	public void setUp() throws Exception {
		Random random = new Random();
		int port = random.nextInt(65535 - 10000 + 1) + 10000;
		instance = new FTPConnectionExtend("127.0.0.1", port, "test", "test", "/test.txt", "test.txt");
		server = new Server(new ServerSocket(port));
	}

	@Test
	public void testOpenConnection() throws InterruptedException, UnknownHostException, IOException {
		String[] data = {"220 OK", "220 Response data", "500 Invalid", "400 error"};
		boolean[] dataStatus = {true, true, false, false};
		for (int i = 0; data.length > i; i++) {
			server.response = data[i];
			server.shouldRun = true;
			serverThead = new Thread(server, "server");
			serverThead.start();
			assertTrue(instance.openConnectionPublic() == dataStatus[i]);
			serverThead.join();	
		}
	}
	
	@After
	public void tearDown() throws IOException {
		instance.close();
	}

}

class FTPConnectionExtend extends FTPConnection {
	public FTPConnectionExtend(
		String host, 
		int port, 
		String username, 
		String password, 
		String fileFrom,
		String fileTo
	) {
		super(host, port, username, password, fileFrom, fileTo);
	}
	public boolean openConnectionPublic () throws UnknownHostException, IOException {
		return super.openConnection();
	}
}

class Server implements Runnable {
	
	public ServerSocket server;
	public boolean shouldRun;
	public boolean running;
	public String response;
	
	public Server (ServerSocket s) {
		server = s;
		shouldRun = true;
		running = false;
		response = "INITIAL";
	}

	@Override
	public void run() {
		try {
			while (shouldRun) {
				SocketWrapper client = new SocketWrapper(server.accept());
				client.input(response);
				shouldRun = false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
