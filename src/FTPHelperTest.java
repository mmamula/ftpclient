import static org.junit.Assert.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

public class FTPHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Test representation type matching for file formats
	 */
	@Test
	public void testGetRepresentationType() {
		String[] data = {
			//file		//representation
			"/x.jpg", 	"I",
			"/x.png", 	"I",
			"/x.img", 	"I",
			"/x.pdf", 	"L",
			"/x.txt", 	"A",
			"/x", 		"L",
		};
		for (int i = 0; data.length > i; i += 2) {
			assertTrue(FTPHelper.getRepresentationType(data[i]).equals(data[i+1]));
		}
	}

	/**
	 * Test port number decoding from PASV responses -> (46,10,23,232,3,44)
	 * Last 2 numbers represent port number when transformed to hex and concatenated
	 */
	@Test
	public void testParsePortFromHex() {
		int[] data = {
			//first //second 	//port
			4, 		15, 		1039,
			255, 	255, 		65535,
			255, 	170, 		65450,
			0, 		0, 			0,
			162, 	81, 		41553,
			0, 		255, 		255,
			2, 		0, 			512,
			1, 		0, 			256
		};
		for (int i = 0; data.length > i; i += 3) {
			assertTrue(FTPHelper.parsePortFromHex(data[i], data[i+1]) == data[i+2]);	
		}
	}

}
