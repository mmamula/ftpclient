import java.io.IOException;
import java.util.Vector;

public class FTPController implements Runnable {
	
	public Vector<FTPConnection> ftpConnections;
	public Vector<Thread> runningThreads;
	public boolean outputStatus = true;
	
	public FTPController () {
		ftpConnections = new Vector<FTPConnection>();
		runningThreads = new Vector<Thread>();
	}
	
	public void addFtpConnection (FTPConnection connection) {
		ftpConnections.add(connection);
	}
	
	public void executeTransfers () {
		for (int i = 0; i < ftpConnections.size(); i++) {
			Thread t = new Thread(ftpConnections.get(i), Integer.toString(i));
			t.start();
			runningThreads.add(t);
		}
	}
	
	public void joinAll () throws InterruptedException {
		for (int i = 0; i < runningThreads.size(); i++) {
			runningThreads.get(i).join();
		}
	}
	
	public void closeFtpConnections () throws IOException {
		for (int i = 0; i < ftpConnections.size(); i++) {
			ftpConnections.get(i).close();
		}
	}
	
	public void printStatus () throws IOException {
		int size = ftpConnections.size();
		String padd = new String(new char[50]).replace("\0", " ");
		for (int i = 0; i < size; i++) {
			ftpConnections.get(i).updateStatus();
			System.out.println(ftpConnections.get(i).status + padd);	
		}
		for (int i = 0; i < size; i++) {
			System.out.print("\33[1A");
		}
	}

	@Override
	public void run() {
		try {
			while (outputStatus) {
				printStatus();
				Thread.sleep(500);	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
